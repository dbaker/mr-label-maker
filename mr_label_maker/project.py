# Copyright © 2020-2022 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys

# from python-gitlab package
import gitlab

USER_ERROR = 1
API_ERROR = 2
BUG = 50

def escape_term(string):
    return ''.join(filter(lambda x: ord(x) >= 32 or ord(x) in {9, 10}, string))

def set2str(s):
    return ','.join(sorted(s))

class GitLabProject:
    def __init__(self, id_, url = 'https://gitlab.freedesktop.org/'):
        self.url = url
        self.id = id_

    def set_token(self, token):
        self.token = token

    def set_dry_run(self, dry_run):
        self.dry_run = dry_run

    def set_label(self, label):
        self.label = label

    def set_state(self, state):
        self.state = state

    def set_ignore_label_history(self, ignore_label_history):
        self.ignore_label_history = ignore_label_history

    def connect(self):
        try:
            self.gl = gitlab.Gitlab(self.url, self.token)
        except gitlab.GitlabError as ex:
            print('Unable to create GitLab server connection: ' + str(ex))
            sys.exit(API_ERROR)

        try:
            self.proj = self.gl.projects.get(self.id)
        except gitlab.GitlabError as ex:
            print('Unable to get information about project: ' + str(ex))
            sys.exit(API_ERROR)

    def apply_labels(self, issue, labels):
        if issue.labels:
            print('old labels: ' + set2str(issue.labels))
        print('new labels: ' + set2str(labels))

        if self.dry_run:
            print('skipping because "dry run" mode is enabled')
            return

        for l in labels:
            issue.labels.append(l)
        issue.save()
        print('applied')

    @staticmethod
    def add_labels(dst, src):
        if type(src) == str:
            if src == "":
                print('empty label')
                raise
            dst.add(src)
            return 1
        if type(src) == list:
            for s in src:
                dst.add(s)
            return len(src)

        print('unhandled type ' + str(type(src)))
        sys.exit(BUG)

    @staticmethod
    def match_starts_with(labels, func, map_, txt):
        cnt = 0
        for key in map_.keys():
            if txt.startswith(func(key)):
                cnt += GitLabProject.add_labels(labels, map_[key])
        return cnt

    @staticmethod
    def match_contains(labels, func, map_, txt):
        cnt = 0
        for key in map_.keys():
            if txt.find(func(key)) >= 0:
                cnt += GitLabProject.add_labels(labels, map_[key])
        return cnt

    @staticmethod
    def match_re(labels, map_, txt):
        cnt = 0
        for key in map_.keys():
            if key.search(txt):
                cnt += GitLabProject.add_labels(labels, map_[key])
        return cnt

    @staticmethod
    def single_area(txt):
        return txt + ': '

    @staticmethod
    def next_area(txt):
        return ' ' + txt + ': '

    @staticmethod
    def tag(txt):
        return '[' + txt + ']'

    @staticmethod
    def asis(txt):
        return txt

    def eval_title(self, labels, title):
        cnt = 0
        cnt += self.match_starts_with(labels, self.single_area, self.areas, title)
        cnt += self.match_contains(labels, self.next_area, self.areas, title)
        cnt += self.match_contains(labels, self.tag, self.areas, title)
        cnt += self.match_contains(labels, self.asis, self.title_codenames, title)
        return cnt

    def eval_description(self, labels, desc):
        return self.match_contains(labels, self.asis, self.desc_codenames, desc)

    def eval_path(self, labels, path):
        return self.match_re(labels, self.paths, path)

    def process_issue(self, issue):
        title = issue.title
        title = title.lower().replace('wip: ', '')
        desc = issue.description
        desc = desc.lower()

        print(issue.attributes['web_url'] + ' | ' + escape_term(issue.title))

        labels = set()

        self.eval_title(labels, title)

        self.eval_description(labels, desc)

        # is this a merge request?
        if issue.attributes.get('sha'):
            chs = issue.changes()
            for change in chs['changes']:
                old_path = change['old_path']
                new_path = change['new_path']
                num = self.eval_path(labels, old_path)
                if old_path != new_path:
                    num += self.eval_path(labels, new_path)
                if num == 0:
                    print('not recognized path: ' + escape_term(old_path))
                    if old_path != new_path:
                        print('not recognized path: ' + escape_term(new_path))

        if not self.ignore_label_history:
            if len(issue.resourcelabelevents.list()) > 0:
                print("has a history of label changes - skipping")
                print('old labels:          ' + set2str(issue.labels))
                print('new labels would be: ' + set2str(labels))

                return

        if len(labels) == 0:
            print('unable to determine labels')
            return

        self.apply_labels(issue, labels)

    def process_issues(self, issue):
        if issue > 0:
            self.process_issue(self.proj.issues.get(issue))
            return

        issues = self.proj.issues.list(as_list=False,
                           labels=self.label,
                           state=self.state,
                           order_by='updated_at')

        for issue in issues:
            self.process_issue(issue)
            print()

    def process_mrs(self, mr):
        if mr > 0:
            self.process_issue(self.proj.mergerequests.get(mr))
            return

        mrs = self.proj.mergerequests.list(as_list=False,
                           labels=self.label,
                           state=self.state,
                           order_by='updated_at')

        for mr in mrs:
            self.process_issue(mr)
            print()
